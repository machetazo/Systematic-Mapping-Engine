# Systematic Mapping Engine

If you found this page, it is very likely that you already know what systematic mapping is. If you dont, here is a link to the essential paper on systematic mapping: [Procedures for performing systematic reviews](http://www.inf.ufsc.br/~aldo.vw/kitchenham.pdf) [1]

This python module is a modest attempt to automate the process of systematic mapping. The final objective is to generate a graph like this one:

![sysmap_image](https://www.researchgate.net/profile/Elisa_Nakagawa/publication/262241783/figure/fig5/AS:296894413721605@1447796552420/Systematic-mapping-on-non-functional-search-based-software-testing-Afzal-et-al-2008.png)

Based on a single search query of any topic.

## Getting Started

To get started, [download the latest release](https://github.com/machetazo/Systematic-Mapping-Engine/releases) and install the following dependencies:
* [summanlp/textrank](https://github.com/machetazo/textrank)
* [ceteri/pytextrank](https://github.com/ceteri/pytextrank)
* [OrganicIrradiation/scholarly](https://github.com/OrganicIrradiation/scholarly)

More detailed installation instructions in [the wiki](https://github.com/machetazo/Systematic-Mapping-Engine/wiki/Installation).

## Contributing

Just do a pull request :)

## Authors

* **Fabián Montero** - *Main developer* - [github](https://github.com/machetazo)

## References

[1] B. Kitchenham, "Procedures for performing systematic reviews," Keele, UK, Keele University, vol. 33, (2004), pp. 1-26, 2004.

Full list of references on [the wiki](https://github.com/machetazo/Systematic-Mapping-Engine/wiki/References).



