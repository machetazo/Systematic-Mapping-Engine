import requests
import os
import scholarly
import shutil
import pathlib
import csv
import sysmapengine.text_processor as tp
import sysmapengine.printer as p
import threading

"""
Talvez sea mejor guardar todos los diccionarios en una lista
luego meter esos diccionarios en un dataframe
luego pasar esa daraframe a un CSV
"""


class Scraper:

    # instanciamos el objeto
    # usamos estos headers para que google crea que somos firefox
    def __init__(self):
        self.download_list = []
        self.headers = requests.utils.default_headers()
        self.headers[
            'User-Agent'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 ' \
                            '(KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
        self.done = False

    def build_csv(self, query, amount):
        """
        Builds a CSV with the results of the scraping and generates a download list
        for the downloader.


        :param query: search query
        :param amount: amount of results to be scraped
        :return: download list
        """
        publications = scholarly.search_pubs_query(query)
        field_names = ['title', 'year', 'author', 'abstract', 'url', 'eprint',
                       'ENTRYTYPE', 'ID', 'publisher', 'number', 'journal',
                       'volume', 'pages', 'booktitle', 'href',
                       'organization','institution', 'automaticSummary']

        counter = 0

        # parsea y valida la cantidad de resultados
        # si falla, pone la cantidad de resultados = 10
        try:
            amount = int(amount)

        except ValueError:
            p.print_red_("Value Error: ", True)
            p.print_red("Amount entered invalid. Setting amount to 10.")
            amount = 10

        # elimina resultados viejos
        if os.path.exists('./results/'):
            shutil.rmtree('./results/')

        # crea directorio para resultados
        pathlib.Path('./results/downloads/').mkdir(parents=True, exist_ok=True)

        # crea csv para resultados
        with open('./results/scrapping_results.csv', 'w') as file:
            csv.register_dialect("toMYSQL", delimiter=";",  quoting=1,
                                 doublequote=1)
            writer = csv.DictWriter(file, fieldnames=field_names,
                                    dialect='toMYSQL')

            # escribe los nombres de las columnas
            writer.writeheader()

            # escribe la info de cada paper en el csv
            for publication in publications:
                if publication is not None and type(
                        publication) is scholarly.Publication:

                    publication.fill()

                    row = publication.__getattribute__('bib')
                    fields = writer.__getattribute__('fieldnames')

                    clean_row = tp.process_row(row)

                    if clean_row is not None:
                        p.print_blue_("Paper fetched: ")
                        print(clean_row)

                        # actualiza la lista de headers para recibir la nueva informacion
                        writer.fieldnames = fields + [item for item in list(clean_row.keys()) if item not in fields]

                        # esto es para debugging
                        print("\t----" + str([item for item in list(clean_row.keys()) if item not in fields]))

                        # mete la row en el csv
                        writer.writerow(clean_row)

                        if 'eprint' in row:
                            self.download_list.append(
                                [
                                 str(clean_row['eprint']),
                                 str(clean_row['title'])
                                ])

                    if counter >= amount:
                        p.print_purple("Done building CSV.", True)
                        print("")
                        self.done = True
                        break
                    else:
                        counter = counter + 1

    def download_files(self):
        """
        Downloads all files in the download list

        :return:
        """
        while not self.done:

            for item in self.download_list:
                p.print_yellow_("Downloading: ")
                p.print_black(item[1])                      # 0 es el url
                self._download_file(item[0], item[1])       # 1 es el nombre
                self.download_list.remove(item)

        p.print_purple("Download Complete.", True)

    def _download_file(self, download_url, file_name):
        """
        Downloads a PDF from a URL and names it after a user-specified title.

        :param download_url: download url
        :param file_name: name of file
        """
        try:
            with open("./results/downloads/" + str(file_name) + ".pdf", 'wb') \
                    as file:
                response = requests.get(download_url, headers=self.headers,
                                        timeout=120)
                file.write(response.content)
                return True

        except:
            p.print_red_("Download failed:", True)
            p.print_red(" Connection timeout after 120 seconds.")
            return False

    def start(self, query, amount):
        fill_up_thread = threading.Thread(target=self.build_csv,
                                          args=(query, amount))

        download_thread = threading.Thread(target=self.download_files)

        fill_up_thread.start()
        download_thread.start()

        fill_up_thread.join()
        download_thread.join()
