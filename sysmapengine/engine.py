import sysmapengine.printer as p
from sysmapengine.scraper import Scraper


def start(query, amount):
    """
    Scraps the web for query results and downloads all papers

    :param query: search query
    :param amount: amount of results
    """
    p.print_cyan("Initializing scrap...", True)
    print("")
    downloader = Scraper()
    downloader.start(query, amount)
    p.print_cyan("Scraping finished.", True)
