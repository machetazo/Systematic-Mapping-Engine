def print_black(text, bold=False):
    """
    Prints text in black using ANSII escape sequences.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[30;1m' + str(text) + '\u001B[0m')
    else:
        print('\u001B[30m' + str(text) + '\u001B[0m')


def print_red(text, bold=False):
    """
    Prints text in red using ANSII escape sequences.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[31;1m' + str(text) + '\u001B[0m')
    else:
        print('\u001B[31m' + str(text) + '\u001B[0m')


def print_green(text, bold=False):
    """
    Prints text in green using ANSII escape sequences.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[32;1m' + str(text) + '\u001B[0m')
    else:
        print('\u001B[32m' + str(text) + '\u001B[0m')


def print_yellow(text, bold=False):
    """
    Prints text in yellow using ANSII escape sequences.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[33;1m' + str(text) + '\u001B[0m')
    else:
        print('\u001B[33m' + str(text) + '\u001B[0m')


def print_blue(text, bold=False):
    """
    Prints text in blue using ANSII escape sequences.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[34;1m' + str(text) + '\u001B[0m')
    else:
        print('\u001B[34m' + str(text) + '\u001B[0m')


def print_purple(text, bold=False):
    """
    Prints text in purple using ANSII escape sequences.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[35;1m' + str(text) + '\u001B[0m')
    else:
        print('\u001B[35m' + str(text) + '\u001B[0m')


def print_cyan(text, bold=False):
    """
    Prints text in cyan using ANSII escape sequences.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[36;1m' + str(text) + '\u001B[0m')
    else:
        print('\u001B[36m' + str(text) + '\u001B[0m')


def print_white(text, bold=False):
    """
    Prints text in white using ANSII escape sequences.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[37;1m' + str(text) + '\u001B[0m')
    else:
        print('\u001B[37m' + str(text) + '\u001B[0m')


def print_black_(text, bold=False):
    """
    Prints text in black using ANSII escape sequences.
    Doesn't print a newline at the end of the string.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[30;1m' + str(text) + '\u001B[0m', end='')
    else:
        print('\u001B[30m' + str(text) + '\u001B[0m', end='')


def print_red_(text, bold=False):
    """
    Prints text in red using ANSII escape sequences.
    Doesn't print a newline at the end of the string.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[31;1m' + str(text) + '\u001B[0m', end='')
    else:
        print('\u001B[31m' + str(text) + '\u001B[0m', end='')


def print_green_(text, bold=False):
    """
    Prints text in green using ANSII escape sequences.
    Doesn't print a newline at the end of the string.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[32;1m' + str(text) + '\u001B[0m', end='')
    else:
        print('\u001B[32m' + str(text) + '\u001B[0m', end='')


def print_yellow_(text, bold=False):
    """
    Prints text in yellow using ANSII escape sequences.
    Doesn't print a newline at the end of the string.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[33;1m' + str(text) + '\u001B[0m', end='')
    else:
        print('\u001B[33m' + str(text) + '\u001B[0m', end='')


def print_blue_(text, bold=False):
    """
    Prints text in blue using ANSII escape sequences.
    Doesn't print a newline at the end of the string.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[34;1m' + str(text) + '\u001B[0m', end='')
    else:
        print('\u001B[34m' + str(text) + '\u001B[0m', end='')


def print_purple_(text, bold=False):
    """
    Prints text in purple using ANSII escape sequences.
    Doesn't print a newline at the end of the string.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[35;1m' + str(text) + '\u001B[0m', end='')
    else:
        print('\u001B[35m' + str(text) + '\u001B[0m', end='')


def print_cyan_(text, bold=False):
    """
    Prints text in cyan using ANSII escape sequences.
    Doesn't print a newline at the end of the string.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[36;1m' + str(text) + '\u001B[0m', end='')
    else:
        print('\u001B[36m' + str(text) + '\u001B[0m', end='')


def print_white_(text, bold=False):
    """
    Prints text in white using ANSII escape sequences.
    Doesn't print a newline at the end of the string.

    :param text: text to be printed
    :param bold: prints in bold if true
    """
    if bold:
        print('\u001B[37;1m' + str(text) + '\u001B[0m', end='')
    else:
        print('\u001B[37m' + str(text) + '\u001B[0m', end='')


