import re
import summa.summarizer
import langdetect


def process_row(row):
    """
    Takes a dictionary an cleans and formats its contents to generate
    a better CSV. It also generates some new columns (year, for example) based
    on the info already in the dictionary.

    :param row: Python dict that represents a row in a CSV file
    :return: the processed row
    """
    if 'abstract' in row:
        row['abstract'] = _process_abstract(row['abstract'])

    if 'eprint' in row:
        row['eprint'] = _process_eprint(row['eprint'])

    return row


def _process_abstract(dirty_abstract):
    """
    Removes strange characters, newlines and from abstract text.

    :param dirty_abstract: raw abstract
    :return: formatted abstract
    """
    dirty_abstract = dirty_abstract.strip()
    clean_abstract = re.sub('\s+', ' ', dirty_abstract)

    return clean_abstract


def _process_eprint(dirty_eprint):
    """
    Fixes the eprint url by removing the 'https://scholar.google.com' prefix.

    :param dirty_eprint: raw eprint url
    :return: fixed eprint url
    """
    return dirty_eprint.replace('https://scholar.google.com', '')


def _detect_language(text):
    """
    Detects the language of a text and returns a string usable by the summa
    summarizing algorithm.

    :param text: text to be analyzed
    :return: language of such text
    """
    text_language = langdetect.detect(text)
    if text_language == 'en':
        return "english"

    elif text_language == 'es':
        return "spanish"

    elif text_language == 'nl':
        return "dutch"

    elif text_language == 'da':
        return "danish"

    elif text_language == 'fi':
        return "finnish"

    elif text_language == 'fr':
        return "french"

    elif text_language == 'de':
        return "german"

    elif text_language == 'hu':
        return "hungarian"

    elif text_language == 'it':
        return "italian"

    elif text_language == 'nb':
        return "norwegian"

    elif text_language == 'pt':
        return "portuguese"

    elif text_language == 'ro':
        return "romanian"

    elif text_language == 'ru':
        return "russian"

    elif text_language == 'sv':
        return "swedish"

    else:
        return "english"


def generate_summary(text, method):
    """
    Generates a summary of a text. (Multiple summarization methods supported)

    :param text: text to be summarized
    :param method: summarization method
    :return: sumarized text
    """

    language = _detect_language(text)
    return _generate_summary_with_summa(text, language)


def generate_keywords(text, method):
    """
    Generates a list of keywords from a text.
    (Multiple summarization methods supported)

    :param text: text to be summarized
    :param method: summarization method
    :return: keywords of the text
    """

    language = _detect_language(text)

    if method == "summa":
        return _generate_keywords_with_summa(text, language)

    # elif method == "pytextrank":
    #     return _generate_keywords_with_pytextrank(text, language)

    else:
        return _generate_keywords_with_summa(text, language)


def _generate_summary_with_summa(text, language):
    """
    Summarizes a text using the summa method.

    :param text: text to be summarized
    :param language: language of such text
    :return: summary of the text
    """
    return summa.summarizer.summarize(text, language=language)


def _generate_keywords_with_summa(text, language):
    """
    Gets keywords of a text using the summa method.

    :param text: text used to generate keywords
    :param language: language of such text
    :return: keywords of the text
    """
    return summa.keywords.keywords(text, language=language, split=True)
